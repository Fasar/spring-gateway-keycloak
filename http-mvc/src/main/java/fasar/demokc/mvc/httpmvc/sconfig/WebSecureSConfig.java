package fasar.demokc.mvc.httpmvc.sconfig;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.authority.mapping.GrantedAuthoritiesMapper;
import org.springframework.security.oauth2.core.oidc.user.OidcUserAuthority;
import org.springframework.security.oauth2.core.user.OAuth2UserAuthority;
import org.springframework.security.web.SecurityFilterChain;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Configuration
@ConditionalOnProperty(value = "debug.keycloak.enable", havingValue = "true", matchIfMissing = true)
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true, jsr250Enabled = true)
public class WebSecureSConfig {
    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http.authorizeHttpRequests((requests) -> {
            requests.anyRequest().permitAll();
        });
        http.oauth2Login(oauth2 ->
                oauth2.userInfoEndpoint(userInfo -> {
                    userInfo.userAuthoritiesMapper(this.userAuthoritiesMapper());
                })
        );
        http.oauth2Client(Customizer.withDefaults());
        return http.build();
    }


    private GrantedAuthoritiesMapper userAuthoritiesMapper() {
        return (authorities) -> {
            Set<GrantedAuthority> mappedAuthorities = new HashSet<>();

            authorities.forEach(authority -> {
                if (OidcUserAuthority.class.isInstance(authority)) {
                    OidcUserAuthority oidcUserAuthority = (OidcUserAuthority) authority;
                    Map<String, Object> attributes = oidcUserAuthority.getAttributes();
                    extractAttributes(attributes, mappedAuthorities);
                } else if (OAuth2UserAuthority.class.isInstance(authority)) {
                    OAuth2UserAuthority oauth2UserAuthority = (OAuth2UserAuthority) authority;
                    Map<String, Object> attributes = oauth2UserAuthority.getAttributes();
                    extractAttributes(attributes, mappedAuthorities);
                }
            });

            return mappedAuthorities;
        };
    }

    private static void extractAttributes(Map<String, Object> attributes, Set<GrantedAuthority> mappedAuthorities) {
        Object springRoles = attributes.get("spring_roles");
        if (springRoles != null) {
            if (springRoles instanceof List) {
                List<String> roles = (List<String>) springRoles;
                roles.forEach(role -> {
                    mappedAuthorities.add(new SimpleGrantedAuthority(role));
                });
            } else {
                mappedAuthorities.add(new SimpleGrantedAuthority((String) springRoles));
            }
        }
    }
}