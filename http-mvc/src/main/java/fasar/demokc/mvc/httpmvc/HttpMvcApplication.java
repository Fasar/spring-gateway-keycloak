package fasar.demokc.mvc.httpmvc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HttpMvcApplication {

	public static void main(String[] args) {
		SpringApplication.run(HttpMvcApplication.class, args);
	}

}
