package fasar.demokc.mvc.httpmvc.controller;

import jakarta.annotation.security.PermitAll;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

@RestController
@Slf4j
public class IndexController {

    @GetMapping(value = "/", produces = "application/json")
    @PermitAll
    public Map<String, Object> hello(HttpServletRequest request, Authentication authentication) {
        Enumeration<String> headers = request.getHeaderNames();
        Map<String, Object> result = new HashMap<>();
        result.put("message", "Hello, from HTTP-MVC Application !");
        Map<String, String> headersMap = new HashMap<>();
        result.put("headers", headersMap);

        while (headers.hasMoreElements()) {
            String header = headers.nextElement();
            headersMap.put(header, request.getHeader(header));
        }

        if (authentication != null) {
            result.put("credentials", authentication.getCredentials());
            result.put("authorities", authentication.getAuthorities());
        }
        return result;
    }

    @Secured("ROLE_ADMIN")
    @GetMapping("/admin")
    public String adminFn() {
        return "Protected for Admin!";
    }

    @Secured("ROLE_USER")
    @GetMapping("/user")
    public String userFn() {
        return "Protected for User!";
    }

    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    @GetMapping("/token")
    public Map<String, Object> index(Authentication authentication) {
        Object details = authentication.getDetails();
        Object principal = authentication.getPrincipal();
        String name = authentication.getName();
        Map<String, Object> res = new HashMap<>();
        res.put("details", details);
        res.put("principal", principal);
        res.put("name", name);
        return res;
    }
}
